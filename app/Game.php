<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Game extends Model
{
    protected $fillable = [
        'id_creater', 'title','winner'
    ];

    public function creater()
    {
        return $this->belongsTo(User::class,'id_creater');
    }   

    public function player0()
    {
        return $this->belongsTo(User::class, 'id_user0');
    }
    
    public function player1()
    {
        return $this->belongsTo(User::class, 'id_user1');
    }    
  
}
