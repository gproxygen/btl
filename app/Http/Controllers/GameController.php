<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Game;
use App\Step;
use App\Events\NewStep;
use App\Events\Games;

class GameController extends Controller
{
    public function __construct() // конструктор, обязательна авторизация
    {
        $this->middleware('auth');
    }

    public function index() // к форме новой игры
    {
        return view('dashboard');
    }
    
    public function demo() // показывает как можно следовать к зависимостям на уровне связей, 
    {         // прописанных в миграциях и моделях БЕЗ дополнительных with на уровне запросов    
        dd(Auth::user()->player1);
        return view('demo');
    }    

    public function allGames($need_broadcast) // получить все игры
    {
        $games = Game::with(['creater','player0','player1'])->orderBy('id','desc')->get();        
        if($need_broadcast){
            broadcast(new Games($games->toJson()));
        }
        return $games;
    }   



    public function new(Request $req) // создание игры и к списку игр
    {    
        // валидация посылки
        $data = $req->validate([
            'title'=>'required|string'
        ]);
        
        try 
        {        
            $newGame = Game::create([
                'id_creater' => auth()->id(),
                'title' => $req->title,
            ]);

            $step = Step::create([
                'id_game' => $newGame->id,
                'id_player' => $newGame->id_creater,
            ]);
        }
        catch (QueryException $e){
            return redirect('home')->with('warning','Невозможно создать сеанс игры');
        }

        $this->allGames(true);
        return redirect('home')->with('success','Сеанс "'.$req->title.'" успешно создан');
    }  

    public function delete(Request $req) // удаление игры её создателем
    {
        $game = Game::where('id', '=', $req->game_id)->first();
        if (auth()->id() == $game->creater->id){
            $step = Game::where('id', '=', $req->game_id)->delete();
            $this->allGames(true);
            return redirect('home')->with('success','Сеанс "'.$game->title.'" успешно удален');    
        }
        else{
            return redirect('home')->with('warning','Сеанс "'.$game->title.'" может быть удален создателем');    
        }
    }

    public function session(Request $req) // к vuejs динамической ленте игр
    {       
        return view('game');
    }

    public function lastStep(Request $req) // взять последний ход
    {

        $game = Game::where('id', '=', $req->game_id)->first();
        $winCode = $game->winner;
        $step = Step::where('id_game', '=', $req->game_id)->first();
        return response()->json(['step'=>$step,'status'=>$winCode]);
    }

    public function makeStep(Request $req) // сделать ход с проверкой
    {        
        $game = Game::where('id', '=', $req->gameId)->first();
        $winCode = $game->winner; // 1=>выиграл первый НОЛИКИ; 2=>выиграл второй КРЕСТИКИ; 3=>НИЧЬЯ;

        if(!($winCode>0)){    // если игра НЕ завершена 
            $step = Step::where('id_game', '=', $req->gameId)->first();
            $status = $step->status;        
            $sep = '-';
            if (auth()->id() == $game->id_user0){
                $sep = 'O';
            }
            if (auth()->id() == $game->id_user1){
                $sep = 'X';
            }

            if ($sep == '-'){   // пытает пойти пользователь, не являющийся игроком
                return response()->json(['step'=>$step,'status'=>-33]); 
            }

            $status[$req->offset] = $sep;        
            $step = Step::where('id_game', '=', $req->gameId)->update([
                'status' => $status, 
                'id_player' => auth()->id()
            ]);
            $winCode = $this->checkWinner($status);
            if($winCode>0){
                Game::where('id', '=', $req->gameId)->update([
                    'winner' => $winCode              
                ]);            
            }        
        }
        $step = Step::where('id_game', '=', $req->gameId)->first();                                    
        broadcast(new NewStep($step));        
        return response()->json(['step'=>$step,'status'=>$winCode]);
    }
    
    public function players(Request $req) // информация об игроках
    {
        $players = Game::where('id', '=', $req->game_id)->with(['creater','player0','player1'])->orderBy('id','desc')->get(); 
        return response()->json($players);
    }    

    public function acceptSecond($game_id) // прием соперника (первый игрок уже ждет)
    {
        $game = Game::where('id', '=', $game_id)->first();
        if($game->id_user0 == auth()->id()){
            return redirect('home')->with('warning','Игры с самим собой запрещены');
        }
        if($game->id_user1){
            return redirect('home')->with('warning','Уже есть соперники, Вы опоздали');
        }        
        
        $game = Game::where('id', '=', $game_id)->update([
            'id_user1' => auth()->id()
        ]);
        Step::where('id_game', '=', $game_id)->update([           
            'id_player' => auth()->id()
        ]);

        $this->allGames(true);
        return;
    } 

    public function accept0(Request $req) // запись первого игрока (blade-static)
    {
        $game = Game::where('id', '=', $req->game_id)->update([
            'id_user0' => auth()->id()
        ]);        
        $this->allGames(true);
        return redirect('home');
    }
    public function accept1($game_id) // запись второго игрока, (get, blade-static)
    {
        $this->acceptSecond($game_id);
        $this->allGames(true);        
        return redirect('home');
    }    

    public function accept0get($game_id) // запись первого игрока (get, vuejs-dynamics, NO redirect)
    {
        $game = Game::where('id', '=', $game_id)->get();
        if($game[0]->id_user0){
            return redirect('home')->with('warning','Первый игрок уже вступил');
        }
        else{
            $game = Game::where('id', '=', $game_id)->update([
                'id_user0' => auth()->id()
            ]);    
        }         
        $this->allGames(true);        
        return;
    }    

    public function accept1get($game_id) // запись второго игрока (get, vuejs-dynamics, NO redirect)
    {
        $this->acceptSecond($game_id);
        $this->allGames(true);        
        return;
    }
    
    public function getList() // отдает все сеансы на домашнюю blade-страницу
    {
        $game = $this->allGames(false);
        return view('home', [
            'games' => $game, 
        ]);
    }   
    
    public function getGames() // отдает json сеансов на динамической верстки
    {
        $game = $this->allGames(false);
        return response()->json($game);
    }       
    
    public function checkWinner($v) { // 0=>ИГРАЕМ ЕЩЕ; 1=>ПОБЕДА НОЛИКОВ; 2=>ПОБЕДА КРЕСТИКОВ; 3=>НИЧЬЯ;
        /* $v (строка 9 символов определяет ходы) содержит 'X','O','*', начало партии = *********
           как матрица индексов:    |0|1|2|
                                    |3|4|5|
                                    |6|7|8|   
        // собираем массив из восьми выигрышных последовательностей  */
        $arr = str_split($v, 3); // 3 горизонтали 
        array_push($arr,
            $v[0].$v[4].$v[8],   // 2 диагонали
            $v[6].$v[4].$v[2],
            $v[0].$v[3].$v[6],   // 3 вертикали
            $v[1].$v[4].$v[7],
            $v[2].$v[5].$v[8]
        ); 
        foreach ($arr as &$m) {
            if ($m=='OOO')
                return 1;   // победа ноликов
            if ($m=='XXX')
                return 2;   // победа крестиков
        }
        if (strripos($v, '*') === false) // ходов нет, ничья
            return 3;        
        return 0;   // играем ещё
    }
}

