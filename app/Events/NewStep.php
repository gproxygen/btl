<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;
use App\Step;

class NewStep implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $step;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Step $step)
    {
        $this->step = $step;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('gamesession.' . $this->step->id_game);
    }

    public function broadcastWith()
    {
        return ["step" => $this->step];
    }    
}
