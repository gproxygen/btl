<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;


class User extends Authenticatable
{
    use Notifiable;

    public function games()
    {
        return $this->hasMany(Game::class,'id_creater');
    }      

    public function player0()
    {
        return $this->hasMany(Game::class,'id_user0');
    }  
    
    public function player1()
    {
        return $this->hasMany(Game::class,'id_user1');
    }      
            
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = []; // разрешаем заполнение всех полей (пустой массив защищаемых)

    // protected $fillable = [
    //     'name', 'email', 'password',
    // ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
}
