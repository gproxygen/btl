@extends('layouts.app')

@section('content')
<div id="app" class="container"> 
    <game-component auth_id="{{ Auth::user()->id }}"/>
</div>
@endsection
