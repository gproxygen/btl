@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">                   
                <div class="card-header">                
                    <form action="{{ route('newGame') }}" method="POST">
                        @csrf
                        <label class="red-font"> <h4>Новая игра</h4> </label>
                        <input name="title" class="form-control" placeholder="введите название">
                        @error('title')
                            <small class="text-danger">{{$message}}</small>
                        @enderror
                        <button class="btn btn-success m-t" type="submit">Создать</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
