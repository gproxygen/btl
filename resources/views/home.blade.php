@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    <h4>Список игр</h4> живая лента                 
                    <a href="{{ route('sessions') }}">тут</a>                
                <div class="list-group">
                @foreach($games as $game)
                <div class="list-group-item m">
                    <div class='row'>
                        <h4>{{ $game->title }}</h4>                        
                        @if (Auth::user()->id == $game->id_creater)
                        <a title="Удалить игру" href="{{ route('game_delete', $game->id) }}" class='static m-l fa fa-trash'></a> 
                        @endif
                    </div>
                    
                    {{-- <div title="Удалить сообщение" class="m-l fa fa-times" @click="deleteMessage(msg.id)"></div> --}}
                    {{-- <input name="fam" type="text" class="form-control" value="{{$game->title}}"> --}}
                    @if ( (Auth::user()->id == $game->id_user0) && (!$game->id_user1))
                        Вы ожидаете соперника
                    @elseif (!$game->id_user0)
                        ожидание соперников
                        <form action="{{ route('accept0') }}" method="POST">
                            <input type="hidden" name="game_id" value="{{ $game->id}}">
                            @csrf
                            <button class='col-4 btn btn-success'>Вступить</button>
                        </form>
                    @elseif (!$game->id_user1)
                        <span class='badge badge-primary'>
                            {{ $game->player0->name }}
                        </span>  ждет соперника
                        <p>
                        <a href="{{ route('accept1', $game->id) }}" class='col-4 btn btn-success btn-sm'>Присоединиться</a>
                        </p>
                    @endif
                    @if ($game->id_user1)
                        <p>
                            <span class='badge badge-primary'>{{ $game->player0->name }}</span>
                            VS
                            <span class='badge badge-danger'>{{ $game->player1->name }}</span>
                        </p>
                        @if ( (($game->id_user1 ==  Auth::user()->id) || ($game->id_user0 ==  Auth::user()->id)) && (!$game->winner) )
                            <a href="{{ route('game_session', $game->id) }}" class='col-4 btn btn-warning btn-sm'>Играть</a>
                        @endif
                        @if ($game->winner>0)
                            <div class='row'>
                                <p class='graph fa fa-trophy'></p>
                                <p>
                                <span class='badge badge-warning'>
                                    @if ($game->winner==1)
                                        {{ $game->player0->name }}
                                    @elseif ($game->winner==2)
                                        {{ $game->player1->name }}
                                    @else
                                        ничья
                                    @endif                                                                
                                    за {{ ($game->updated_at->timestamp -$game->created_at->timestamp) }} сек
                                </span>
                                </p>
                            </div>                                                      
                        @endif
                    @endif                     
                    <p><small> создал {{ $game->creater->name }}  {{ $game->created_at }} </small></p>
                </div>
                <br>
                @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
