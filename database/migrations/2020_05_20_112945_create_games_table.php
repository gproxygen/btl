<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGamesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('games', function (Blueprint $table) {
            $table->id();
            $table->string('title');                                         
            $table->unsignedBigInteger('winner')->nullable();                                         

            $table->unsignedBigInteger('id_creater')->nullable(false);
            $table->unsignedBigInteger('id_user0')->nullable();
            $table->unsignedBigInteger('id_user1')->nullable();                                    
            $table->unsignedBigInteger('id_file0')->nullable();
            $table->unsignedBigInteger('id_file1')->nullable();
                      
            $table->foreign('id_user0')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('id_user1')->references('id')->on('users');            
            
            $table->foreign('id_creater')->references('id')->on('users');
            $table->foreign('id_file0')->references('id')->on('files');            
            $table->foreign('id_file1')->references('id')->on('files');          
                        
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('games');
    }
}
