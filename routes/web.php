<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/sessions', function () {return view('games');})->name('sessions');

Route::get('/home', 'GameController@getList')->name('home');
Route::get('/dash', 'GameController@index')->name('newGameData');
Route::post('/new', 'GameController@new')->name('newGame');

Route::post('/accept0', 'GameController@accept0')->name('accept0');
Route::get('/accept1/{game_id}', 'GameController@accept1')->name('accept1');

Route::get('/accept0get/{game_id}', 'GameController@accept0get');
Route::get('/accept1get/{game_id}', 'GameController@accept1get');

Route::get('/game/{game_id}', 'GameController@session')->name('game_session');
Route::get('/game/players/{game_id}', 'GameController@players');
Route::get('/game/lastStep/{game_id}', 'GameController@lastStep');
Route::post('/game/makeStep/{game_id}', 'GameController@makeStep');
Route::get('/games', 'GameController@getGames');

Route::get('/delete/{game_id}', 'GameController@delete')->name('game_delete');